#!/usr/bin/env python
# Created by Christopher Anelli, August 19 2020
# Code to read in a database of customer complaints, outputting a list for each year
# and complaint category of the number of complaints, number of companies, and fraction
# of company with the most complaints.
# Takes as arguments the paths to the .csv input file and the output file.
# Example:
# > python ./src/consumer_complaints.py ./input/complaints.csv ./output/report.csv

import sys
from datetime import date
from operator import itemgetter
import csv

# Complaints stored in a dictionary of dictionaries.
COMPLAINT_STORE={}

def consumer_complaints(input_file_path, output_file_path):
    read_in(input_file_path)
    output(output_file_path)


# Using the python csv module, the CFPB dataset is read in.  The global variable, 
# COMPLAINT_STORE is a map of maps (dictionaries), where the key is the year 
# and product type pairs to an inner-map, where the key-value pairing is the
# company name to a counter for the number of complaints filed against that company.

def read_in(csv_file_path):
    infile = open(csv_file_path, 'r')
    # header = _file.readline() # Get Header
    # Read in using CSV DictReader, by default row names are taken from the header
    reader = csv.DictReader(infile)

    # For this project are interested in the Date (year), Product, and Company
    for row in reader:
        #print date.fromisoformat(row['Date received'])
        year = int(row['Date received'].split('-')[0]) # extract year from date
        product = row['Product'].lower()
        company = row['Company'].lower()
        #print year, product, company
        key = (year, product)
        
      
        #
        # ADD COMPLAINT to COMPAINT_STORE
        #
        if key not in COMPLAINT_STORE:
            COMPLAINT_STORE[key] = {company: 1}
        else:
            # Check if company already exists for this year and product
            if company not in COMPLAINT_STORE[key]:
                COMPLAINT_STORE[key][company] = 1
            #If company already input, increment complaint counter by 1
            else:
                COMPLAINT_STORE[key][company] += 1

    infile.close()

# Outputs a report.csv file, containing for each year and product type the 
# total number of complaints, the number of companies, and the highest percentage 
# of complaints filed against a single company.  
# Output is ordered by year and then alphabetically by product type, and formatted 
# so that percentages are rounded to the nearest whole number, and product types 
# containing a comma are printed inside quotations.

def output(file_path):
    outfile = open(file_path, 'w')
    
    keys = COMPLAINT_STORE.keys()
    
    # Order by Year And Product Type
    ordered_keys = sorted(keys, key=itemgetter(1,0))
    
    for key in ordered_keys:
        #print key
        year = key[0]
        product = key[1]
        number_of_companies = len(COMPLAINT_STORE[key].keys())
        # Test Print Out Names of Companies
        #for company in sorted(COMPLAINT_STORE[key].keys()):
        #    print company + ";" ,
        #print "\n"
        total_complaints = sum(COMPLAINT_STORE[key].values())
        max_complaints = max(COMPLAINT_STORE[key].values())

        #
        # Format Output
        #
        percentage = int(round( (100.0 * max_complaints) / total_complaints)) # Rounded to the nearest whole number
        
        if "," in product: # Add quotation marks if product contains comma.
            product = '"' + product + '"'
        
        #print year, product ,number_of_companies, total_complaints, percentage
        outfile.write('{0},{1},{2},{3},{4}\n'.format(product, year, total_complaints, number_of_companies, percentage))

    outfile.close()
                  

if __name__ == '__main__':
    if len(sys.argv)!= 3:
        "Please provide input and output file paths"
    else:
        consumer_complaints(sys.argv[1], sys.argv[2])

