/*
* Created by Christopher Anelli, August 20, 2020
* Code to read in a database of customer complaints, outputting a list for each year
* and complaint category of the number of complaints, number of companies, and fraction
* of company with the most complaints.
* Takes as arguments the paths to the .csv input file and the output file.
* Example:
*/

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <cmath>
#include <functional>

using namespace std;

vector<string> Tokenize(string line);
void ReadIn(string path);
void Output(string path);
int FindCol(vector<string> & header_tokens, string column_name);
int CountChar(string string, char c);
bool cmpProductYear(pair<int, string> first, pair<int, string> second);


//Map is ordered alphabetically by product and by year.
//bool (*less_pt)(string,string) = less_string;
bool (*fn_pt)(pair<int,string>, pair<int,string>) = cmpProductYear;
//map::key_compare = less<T>;
map<pair<int, string>, map<string, int>, bool(*)(pair<int,string>,pair<int,string>) > COMPLAINT_STORE(fn_pt);

int main(int argc, char * argv[]){
    if(argc != 3){
        cout << "Please pass paths for the input csv file and output report file" << endl;
    }
    else {
        string input_path = argv[1];
        string output_path = argv[2];
        ReadIn(input_path);
        Output(output_path);
    }
}

void ReadIn(string path){
    string header;
    string line;
    string nextline;
    ifstream file;
    file.open(path);
    //Check if file was found
    if(!file.is_open()){
        throw std::runtime_error("Could not open file");
    } else {
        // Get Header
        getline(file, header);
        vector<string> header_tokens = Tokenize(header);
        // Use Header to Find Columns for the Date, Product, and Company
        int date_column = FindCol(header_tokens, "Date received");
        int product_column = FindCol(header_tokens, "Product");
        int company_column = FindCol(header_tokens, "Company");
        
        // Read in line by line
        while( getline(file, line)  ){
            //getline(file, line);
            while( CountChar(line, '"') %2 != 0){ //Check for unclosed quotation marks
                getline(file, nextline);
                line += nextline;
            }
            //cout << line << endl;
            //cout <<CountChar(line, '"') << endl;
            vector<string> tokens = Tokenize(line);
            string date = tokens[date_column];
            string product = tokens[product_column];
            string company = tokens[company_column];
            
            // Parse Year and convert string to lower case
            int year = stoi(date.substr(0, date.find('-')));
            transform(product.begin(), product.end(), product.begin(), ::tolower);
            transform(company.begin(), company.end(), company.begin(), ::tolower);
            //cout << year << product << company << endl;
            
            // Enter Data into COMPAINT_STORE
            pair<int, string> key(year, product);
            if(COMPLAINT_STORE.count(key) == 0){
                map<string,int> inner_map;
                COMPLAINT_STORE[key]= inner_map;
                COMPLAINT_STORE[key][company] = 1;
            } else {
                if(COMPLAINT_STORE[key].count(company) == 0){
                    COMPLAINT_STORE[key][company] = 1;
                } else {
                    COMPLAINT_STORE[key][company] +=1;
                }
            }
        }
    }
    file.close();
}

/*
 * Custom Tokenizer to split string up at the commas, but ignoring those
 * within quotation marks.
 */
vector<string> Tokenize(string str){
    vector<string> tokens;
    size_t pos = str.find_first_of("\",");
    while(pos  != string::npos){
        //If quotation, finds first comma following the close of the quotation.
        if (str[pos]== '"'){
            pos = str.find_first_of('"', pos+1);
            pos = str.find_first_of(',', pos+1);
        }
        //cout << str.substr(0,pos) << endl;
        tokens.push_back( str.substr(0,pos));
        str = str.substr(pos+1);
        pos = str.find_first_of("\",");
    }
    tokens.push_back(str);
    return tokens;
}

int FindCol(vector<string> & header, string column_name){
    for( int i =0; i < header.size(); i++){
        if(header[i]==column_name) return i;
    }
    cout << "Warning " << column_name << "is missing from the input file";
    return -1;
}


void Output(string path){
    ofstream file;
    file.open(path);
    
    if(!file.is_open()){
        cout << "Unable to write to " << path << endl;
    } else {
        // map is alreay ordered, loop over it
        map<pair<int,string>, map<string, int> >::iterator it;
        for (it = COMPLAINT_STORE.begin(); it != COMPLAINT_STORE.end(); it++){
           
            // Extract year and product from the key
            int year = it->first.first;
            string product = it->first.second;
            int number_of_companies = it->second.size();
            //cout << "Number of companies" << number_of_companies << endl;
            
            map<string,int> inner_map = it->second;
            
            // Sum up the complaints for all the companies.
            //int total_complaints = accumulate(begin(it->second), std::end(it->second), 0, [] (int value, const std::map<string, int>::value_type& p){return value + p.second; });
            int total_complaints = 0;
            int max_complaints = 0;
            
            // loop over inner-map to calculate total complaints
            map<string, int>::key_compare mycomp = inner_map.key_comp();
            //cout << inner_map.begin()->first << " vs " << inner_map.end()->first << endl;
            for (auto inner_it = inner_map.cbegin(); inner_it != inner_map.cend(); ++inner_it){
                //cout << inner_it->first << endl;
                max_complaints = max(max_complaints, inner_it->second);
                total_complaints += inner_it->second;
            }
            
            
            //cout << mycomp(inner_map.begin()->first, inner_map.end()->first) << endl;
            /*
            //map<string, int>::iterator it2;
            //cout << it->second.begin()->first << endl;
            
            //max_complaints = accumulate(begin(it->second), end(it->second), 0, [](int previous, const std::pair<string, int>& p)
            //                { return previous + p.second; });
            
            //map<string, int> inner_map = it->second;
            //map<string, int>::iterator inner_it;
         
             
            //for(inner_it = inner_map.begin(), inner_it != inner_map.end(); inner_it++ ){
                max_complaints = max(max_complaints, inner_it->second);
                total_complaints += inner_it->second;
            }
             */
            
            // FORMAT
            int percentage = round(100.0 * max_complaints/ total_complaints);
            //cout << product << year << total_complaints << number_of_companies << percentage << endl;
            file << product << year << total_complaints << number_of_companies << percentage << endl;
        }
        
    }
    file.close();
}

bool cmpProductYear(pair<int, string> first, pair<int, string> second){
    // Compare Names, use Year as the tie breaker
    if(first.second == second.second){
        return first.first < second.first;
    }
    return first.second < second.second;
}

// Counts the number of occurences of a substring in string
int CountChar(string str, char c){
    int count =0;
    int pos = str.find(c);
    while(pos != string::npos){
        count ++;
        pos = str.find(c, pos+1);
    }
    return count;
}
