# Consumer Complaints

As a solution to the consumer complaints challenge, this code reads in a complaints file in the 
CFPB's technical documentation [format](https://cfpb.github.io/api/ccdb/fields.html) and outputs a report 
file that for each year and product type contains: the number of complaints, 
the number of companies receiving a complaint, and the highest percentage of complaints filed against a single company.

## Execution

This code can be run directly by calling the run.sh script:  
> ./run.sh  

Inside this script, it executes the python code **consumer_complaints.py** and takes as arguments paths to the 
input csv file and the output report file.  
> python ./src/consumer_complaints.py ./input/complaints.csv ./output/report.csv
