#!/bin/bash
#
# Use this shell script to compile (if necessary) your code and then execute it. Belw is an example of what might be found in this file if your program was written in Python 3.7
# python3.7 ./src/consumer_complaints.py ./input/complaints.csv ./output/report.csv

python ./src/consumer_complaints.py ./input/complaints.csv ./output/report.csv

#python ./src/consumer_complaints.py ./insight_testsuite/test_1/input/complaints.csv ./insight_testsuite/test_1/output/report.csv

#For c++
#g++ -o consumer_complaints.exe src/consumer_complaints.cc
#./consumer_complaints.exe ./input/complaints.csv ./output/report.csv

#Deprecated
#python ./src/prepare_consumer_complaints.py ./input/complaints.csv