#!/usr/bin/env python3
# Created by Chris Anelli
# Predicts whether a partient will be readmitted to the hospital.
# Example Execution
# ./readmission_classifier.py classifier_cfg

import os
import sys
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.backends.backend_pdf
import math
import csv
import configparser
#from scipy.stats import poisson
from scipy.stats import binom
from sklearn.utils import shuffle
from sklearn import preprocessing
from sklearn.linear_model import SGDClassifier
#from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import roc_curve
from sklearn.metrics import roc_auc_score
from matplotlib.backends.backend_pdf import PdfPages
from matplotlib.collections import PatchCollection
from matplotlib.patches import Rectangle


def readmission_classifier():

    # Create pdf file to store exploratory figures
    global pdf
    pdf = matplotlib.backends.backend_pdf.PdfPages(config["Paths"]["output"])

    # Read data into panda dataframe
    df = read_in_data()
    
    # Preprocess data into format for classifier
    df_X, df_Y = preprocess(df)
    
    #Train and Test Classifier
    clf = train_test_classifier(df_X, df_Y)
    
    pdf.close()  # Close Exploratory pdf
    

# Trains logistic regression classifier with gradient descent
# Penalty is l2
def train_test_classifier(df_X, df_Y):
    # Convert to format expected by SGDClassifier,
    # split  into training and testing datasets
    X = df_X.to_numpy()
    Y = np.transpose(df_Y.to_numpy())[0]
    #shuffle(X,Y)
    offset = int(X.shape[0] * 0.66)
    X_train, Y_train = X[:offset], Y[:offset]
    X_test, Y_test = X[offset:], Y[offset:]
    
    # Classifier
    clf = SGDClassifier(loss = "log", penalty="l2", alpha=0.001, max_iter=100, random_state=1) #random_state=1 for reproducability
    #clf = KNeighborsClassifier(n_neighbors=1)
    
    # Train
    clf.fit(X_train,Y_train)
    
    # Test
    #Z = clf.decision_function(X_test)
    #plot_histogram("Decision Function", Z)
    
    probs = clf.predict_proba(X_test)
    probs = probs[:, 1]
    plot_histogram("Probability Readmitted", probs)
    
    auc = roc(probs, Y_test)
    print("The classifier's AUC score is: {:.2f}".format(auc))
    
    # Change threshold
    #Y_pred = clf.predict(X_test)
    #Y_pred = Z > -2.00
    Y_pred = probs > 0.125
    Y_pred = -1 * (Y_pred == False) + 1 * (Y_pred == True)
    classifier_metrics(Y_pred, Y_test)

    #print(clf.coef_)
    return clf
    
#Plots ROC Curve and calculates AUC
def roc(probs, Y_test):
    n_fpr, n_tpr, _ = roc_curve(Y_test, probs)
    plot_roc_curve(n_fpr, n_tpr)
    return roc_auc_score(Y_test, probs)
    
    
# Handles formatting the data into input on which the classifier can be trained.
# Parses strings into values, turns categorical data into binary features, and
# performs feature scaling
def preprocess(df):
    df_X , df_Y = pd.DataFrame(), pd.DataFrame()
    
    # Read Mappings from the configuration file
    mappings = {}
    for col in config["Maps"]:
        mappings[col] = eval(config["Maps"][col])

    # Filter Expired Patients (they cannot be readmitted)
    df = filter(df)
    
    # Process Y (Binary Class)
    df_Y = process_y(df, df_Y)
    
    # Categorical
    df_X = process_categorical(df, df_X, mappings, df_Y)
    
    # Numerical
    df_X = process_numerical(df, df_X, mappings, df_Y)
    
    # Diagnoses
    df_X = process_diagnoses(df, df_X, df_Y)
    
    # Drugs (To reduce # of features, only indicates if patient is on the drug)
    df_X = process_drugs(df, df_X, df_Y)

    print("Features:", list(df_X))
    
    # Remove Features
    df_X = ignore_features(df_X)
    
    return df_X, df_Y
    
# Filter from the datat frame entries where column value is in the cut list
def filter(df):
    print("Filtering expired patients")
    for filter in config["Filter"]:
        col_title, cut_list = eval(config["Filter"][filter])
        cut_list = eval(cut_list) # String to list
        return df[~df[col_title].isin(cut_list)]
        
# Handles the preprocessing for the Categorical Features.
# Most categories are specified in the configuration file
def process_categorical(df, df_X, mappings, df_Y):
    print("Pre-processing Categorical Features")
    for category in config["Categorical"]:
        col , select_features = eval(config["Categorical"][category])
        select_features = eval(select_features)
        features = pd.DataFrame()
        if(col in mappings): features = pd.get_dummies(df[col].map(mappings[col]))
        else: features = pd.get_dummies(df[col])
        features = features[ select_features]
        explore_categorical(features, df_Y)
        df_X = pd.concat([df_X, features], axis=1)
    return df_X
    
# By default standardized the numerical distributions, but many of the distributions
# were non-Gaussian and would have been intereting to explore further with extra time.
# Again cells are specified in the configuration file
def process_numerical(df, df_X, mappings, df_Y):
    print("Pre-processing Numerical Features")
    scaler = preprocessing.StandardScaler()
    for col in config["Numerical"]:
        if(col in mappings):
            df_X[col] = df[col].map(mappings[col])
            explore_numerical(col, df_X[col], df_Y) # Explore before scaling
            df_X[col] = scaler.fit_transform(df_X[[col]])
        else:
            explore_numerical(col, df[col], df_Y) # Explore before scaling
            df_X[col] = scaler.fit_transform(df[[col]])
    return df_X

# Handles the diagnoses features and combining multiple columns
def process_diagnoses(df, df_X, df_Y):
    print("Pre-processing Diagnosis Features")
    # Map icd to css code
    icd_to_css_map  = build_icd_css_map()
    column_list = eval(config["Diagnosis"]["column_list"])
    df_X = combine_diagnosis_columns(df, df_X, df_Y, column_list, icd_to_css_map)
    return df_X
    
# Handles Processing Drug Features
def process_drugs(df, df_X, df_Y):
    print("Pre-processing Drug Features")
    mapping = eval(config["Drugs"]["mapping"])
    drug_list = eval(config["Drugs"]["list"])
    for drug in drug_list:
        df_X[drug] = df[drug].map(mapping)
    
    # Explore
    explore_categorical(df_X[drug_list], df_Y)
        
    return df_X

# Handles Processing Y
def process_y(df, df_Y):
    df_Y['readmitted'] = df['readmitted'].str.lower().map({'yes':1, 'no':-1})
    
    explore_categorical(df_Y = df_Y)
    
    return df_Y
        
# Reading from the csv file, returns a dictionary mapping the
# icd number to the corresponding css code.
def build_icd_css_map():
    map = {}
    file = open(config["Paths"]["icd_to_css"], 'r')
    reader = csv.DictReader(file)
    for row in reader:
        map[row['icd']] = row['ccs_desc']
    return map

# Code for combining the Diagnosis Categories
# Could be cleaner, ran into some issues with NaN when trying to add the three columns
def combine_diagnosis_columns(df, df_X, df_Y, col_list, map):
    unique_diagnoses = []
    for col in col_list:
        unique_diagnoses += list(df[col].map(map).unique())
    unique_diagnoses = set(unique_diagnoses)
    
    if np.nan in unique_diagnoses: unique_diagnoses.remove(np.nan) # Remove NaN
   
    for dummy in unique_diagnoses:
        df_X[dummy] = 0
        
    for col in col_list:
        diagnosis_features = pd.get_dummies(df[col].map(map))
        for dummy in unique_diagnoses:
            if dummy not in list(diagnosis_features): continue
            df_X[dummy] += diagnosis_features[dummy]
            
            
    # Explore Data
    explore_categorical(df_X[unique_diagnoses], df_Y)
    return df_X
    
# User specifies features to ignore,
# for example if they have very few entries.
def ignore_features(df_X):
    ignore_feature_list = eval(config["Ignore"]["feature_list"])
    return df_X.drop(columns = ignore_feature_list)

    
# Reads data from csv file into panda dataframe
def read_in_data():
    return pd.read_csv(config["Paths"]["data"])


# Produces overlayed histograms for readmitted patients in red
# and non-readmitted patients in blue.
def explore_numerical(col, df_X, df_Y):
    Y = np.transpose(df_Y.to_numpy())[0]
    #for col in config["Numerical"]:
    X_Y = np.vstack( (df_X.to_numpy(), Y) )
    x_pos = [ x_y[0] for x_y in X_Y.T if x_y[1] == 1]
    x_neg =  [ x_y[0] for x_y in X_Y.T if x_y[1] == -1]
    overlay_histograms(col, x_neg, x_pos)
        
# For events where category is positive, produces pie chart of readmitted and non-readmitted patients.
# If no category is specified, produces pie chart of all events.
def explore_categorical(df_X = None, df_Y=None):
    Y = np.transpose(df_Y.to_numpy())[0]
    
    labels = []
    percentages = []
    errors = []
    
    if df_X is not None:
        for col in df_X.columns:
            labels.append(col)
            pos = np.sum(np.multiply(df_X[col].to_numpy() ==1, Y==1))
            neg = np.sum(np.multiply(df_X[col].to_numpy() ==1, Y==-1))
            n = pos + neg
            p , err = CalcPercentage(pos, n)
            percentages.append(p)
            errors.append(err)
    # All Events Must be handled seperately
    else:
        labels.append("All Events")
        pos = np.sum(Y==1)
        neg = np.sum(Y==-1)
        n = pos + neg
        p , err = CalcPercentage(pos, n)
        percentages.append(p)
        errors.append(err)
        
    plot_hbar(labels, percentages, errors)
    
# Given a column, calculate the fraction of events with the feature present
# that are positive in Y.
def CalcPercentage(pos, n):
    if(n==0): return 0, 1 # Case where no events are of this category
    p = float(pos)/n # % Positive
    mean, var = binom.stats(n, p, moments='mv') #Vairance = np(1-p)
    p_err = math.sqrt(var)/n #error on p
    return p, p_err
    

# Plot Horizontal Bar Chart
def plot_hbar(labels, percentages, errs):
    fig, ax = plt.subplots()
    
    # Sort by % positive
    percentages, errs, labels = zip(*sorted(zip(percentages, errs, labels)))
    y_pos = np.arange(len(labels))
    
    errorboxes = [Rectangle((x - xe, y-0.39), 2*xe, 0.78)
                  for x, y, xe in zip(percentages, y_pos, errs)]
                  
    pc = PatchCollection(errorboxes, hatch='////', facecolor='none', alpha=0.6, zorder=2)
    ax.add_collection(pc)

    ax.barh(y_pos, percentages, align='center', color='orange', zorder=0)
    ax.set_yticks(y_pos)
    ax.set_yticklabels(labels)

    ax.set_xlabel('Percentage Readmitted')
    plt.xlim([0,1])
    
    pdf.savefig(fig) # global, multipage pdf
    plt.close()
    
    
    
# Plot Histogram
def overlay_histograms(title, x_neg, x_pos):
    n_bins = 10
    fig, ax = plt.subplots()
    n, bins, patches = ax.hist([x_neg,x_pos], n_bins, histtype='step', fill=False, color = ['b', 'r'], density=True, label=["Not Readmitted", "Readmitted"])
    ax.set_ylim([0, 1.2*max( max(n[0]) , max(n[1]) )]) # Buffer for legend
    plt.legend(loc='upper left')
    plt.ylabel("Normalized")
    plt.xlabel(title)
    plt.title(title)
    
    pdf.savefig(fig) # global, multipage pdf
    plt.close()
    
def plot_histogram(title, x):
    n_bins = 20
    fig, ax = plt.subplots()
    n, bins, patches = ax.hist(x, n_bins, density=True)
    plt.ylabel("Normalized")
    plt.xlabel(title)
    plt.title(title)
    
    pdf.savefig(fig) # global, multipage pdf
    plt.close()
    
#Plot Pie Charts
"""
def pie_chart(title, wedges):
    fig, ax = plt.subplots()
    labels = ["Not Readmitted", "Readmitted"]
    colors = ['b', 'r']
    ax.pie(wedges, labels=labels, autopct='%1.1f%%',
        shadow=True, startangle=90, colors = colors)
    ax.axis('equal')
    plt.title(title)
    
    n = sum(wedges)
    p = wedges[1]/n
    mean, var = binom.stats(n, p, moments='mv') #Vairance = np(1-p)
    p_error = math.sqrt(var)/n #error on p
    
    ax.text(0.75, -1., r'*$N$ = {}'.format(n), fontsize=12)
    ax.text(0.75, -1.1, r' $N_+$ = ({:.1f} $\pm$ ${:.1f}$)%'.format(100.0*p, 100.0*p_error), fontsize=12)
    #ax.text(1.05, -1.2, r'$N_-$ = {:.1f}%'.format(100.0*wedges[1]/sum(wedges)), fontsize=12)
    pdf.savefig(fig) # global, multipage pdf
    plt.close()
"""
    
#Plot ROC Curve
def plot_roc_curve(n_fpr, n_tpr):
    fig, ax = plt.subplots()
    plt.plot(n_fpr, n_tpr, marker='.', color='orange')
    plt.ylabel("True Positive Rate")
    plt.xlabel("False Postive Rate")
    plt.title("ROC Curve")
    
    # For comparison plot y = x
    x = [i/20.0 for i in range(0,21)]
    y = x
    plt.plot(x, y, linestyle='--', color='black')
    #plt.show()
    pdf.savefig(fig)
    plt.close()
    
#Classifier Metrics
def classifier_metrics(Y_pred, Y_test):
    accuracy = np.mean(Y_pred == Y_test)
    tp = 1.0*np.sum(np.multiply(Y_pred==1 , Y_test==1))
    fp = 1.0*np.sum(np.multiply(Y_pred==1 , Y_test==-1))
    fn = 1.0*np.sum(np.multiply(Y_pred==-1 , Y_test==1))
    
    tpr = tp/np.sum(Y_test==1)
    fpr = fp/np.sum(Y_test==-1)

    precision = tp / (tp + fp)
    recall = tp / (tp + fn)
    
    print("Classifier Results:")
    
    print ("Classifier's overall accuracy is: {:.2f}".format(accuracy))
    print ("Classifier's true positive rate is: {:.2f}".format(tpr))
    print ("Classifier's false positive rate is: {:.2f}".format(fpr))
    print ("Classifier's precision is: {:.2f}".format(precision))
    print ("Classifier's recall is: {:.2f}".format(recall))
    
if __name__ == '__main__':
    config_file = sys.argv[1]
    config = configparser.ConfigParser()
    config.read(config_file)
    readmission_classifier()
