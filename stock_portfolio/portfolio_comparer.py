#!/usr/bin/env python3
# Created by Christopher Anell
# Compares the daily and monthly returns of two portfolios, as well as the
# correlation in daily returns.  Portfolio composition and the time frame
# are specified in the configuration file, portfolios.cfg.
# > ./portfolio_comparer.py portfolio.cfg

import os
import sys
import configparser
from datetime import timedelta, date, datetime
import numpy as np
import pandas as pd
from math import *
from functools import reduce

# Compares two portfolios over a range of dates (defined from configuration file)
# Outputs daily returns, total return, and correlation.
# Utilizes historical data read into pandas dataframe.
def portfolio_comparer(portfolios):
    # Read in Historical Data
    df = read_in_data()

    # Add portfolio values to the dataframe
    for portfolio in portfolios: add_portfolio(df, portfolio)
    
    # Select Date Range
    df_range = select_dates(df)
    
    #Output Daily and Total returns in the portfolios
    portfolio_names = [portfolio.name for portfolio in portfolios]
    daily_returns(df_range, portfolio_names)
    total_returns(df_range, portfolio_names)
    
    # Calculate correlation in the returns of the first two portfolios.
    correlation(df_range, portfolio_names[0], portfolio_names[1])
    
    
# Return filtered dataframe for entries within the date range
def select_dates(df):
    df_range = df[(df.index >= START_DATE) & (df.index <= END_DATE)]
    return df_range

# Daily Percent Changes
def daily_returns(df, portfolio_names):
    print("DAILY RETURNS:")
    print(df[portfolio_names].pct_change())
    #print(df[[cfg.portfolio_a[0], cfg.portfolio_b[0]]].pct_change())
    print()
    
# Total Percent Change
def total_returns(df, portfolio_names):
    print("TOTAL RETURN:")
    df_bookend = df.iloc[[0, -1]]
    print(df_bookend[portfolio_names].pct_change())
    #print(df_bookend[[cfg.portfolio_a[0], cfg.portfolio_b[0]]].pct_change())
    print()

        
# Correlation
# Calculate the unbiased pearson correlation coefficient between the returns of the two portfolios
def correlation(df, portfolio_a, portfolio_b):
    print("CORRELATION:")
    corr = df[portfolio_a].pct_change().corr(df[portfolio_b].pct_change())
    print("The correlation in the daily returns of {} and {} is {:.2f}".format(portfolio_a,portfolio_b, corr))
    
    print()
    

# Read in historical stock data from the CSV files.
# Assume files are named following the convention [TICKER_SYMBOL].csv
# Merge into a single df, using the date
def read_in_data():
    dfs = {}
    directory = config["Data"]["path"]
    date_column = config["Col"]["date"]
    close_adj_column=config["Col"]["close_adj"]
    
    #directory = cfg.data_path
    for f in os.listdir(directory): #Path to directory containing historical stock quotes
        stock_symbol = os.path.splitext(f)[0]
        # read date column in as dates
        dfs[stock_symbol] = pd.read_csv(os.path.join(directory,f), usecols=[date_column, close_adj_column], parse_dates=[date_column])
        
        #Rename "adj_close" column to stock symbol
        dfs[stock_symbol] = dfs[stock_symbol].rename(columns={close_adj_column : stock_symbol})

    # Merge together the individual stock df using the "date" column
    df = reduce( lambda left, right: pd.merge(left, right, on=date_column), dfs.values())
    
    # Use the "date" column as the index
    df = df.set_index(date_column)
    return df
    
# Add portfolios as a new column to the dataframe.  If percent flag is enabled,
# normalize to unity.
def add_portfolio(df, portfolio):
    stocks = portfolio.stocks
    # If percent flag, normalizes stock based on prices at START_DATE
    if(portfolio.percent_flag):
        stocks = normalize(df, portfolio.stocks, portfolio.percent_flag)
    
    # Add portfolio value to the data frame.
    df[portfolio.name] = 0
    for num, stock in stocks: df[portfolio.name] += num * df[stock]

# Normalizes the number of stocks so that the total value of the portfolio is 1.
def normalize(df, stocks, percent_flag = False):
    stocks_norm = []
    if(percent_flag):
        stocks_norm = [ (percent / df.loc[START_DATE][stock], stock) for percent, stock in stocks ]
    else:
        total = sum([ num * df.loc[cfg.start_date][stock] for num, stock in stocks ])
        stocks_norm = [ (num / total, stock) for num, stock in stocks ]
    return stocks_norm

# Simple class to hold basic information about the portfolio, such as its name,
# and a list of the stocks and number of stocks contained.
class Portfolio:
    def __init__(self, name, stocks, percent_flag=False):
        self.name = name
        self.stocks = stocks
        self.percent_flag = percent_flag

#Simple parser to create a Portfolio object from the configuration file
def create_portfolio(portfolio_cfg):
    #("Name", "Stocks", "By_Percentage_Flag")
    tup = eval(portfolio_cfg)
    return Portfolio(tup[0], eval(tup[1]), bool(tup[2]))
    

if __name__ == '__main__':
    config_file = sys.argv[1]
    config = configparser.ConfigParser()
    config.read(config_file)
    # define start and end dates as global variables
    START_DATE=datetime.strptime(config["Dates"]["start_date"], "%Y-%m-%d")
    END_DATE=datetime.strptime(config["Dates"]["end_date"], "%Y-%m-%d")
    # list of portfolios in the configuration file
    portfolios = [create_portfolio(config["Portfolios"][portfolio]) for portfolio in config["Portfolios"]]
    portfolio_comparer(portfolios)

