/*
 * Declare Distance, Location, and Time variables for use in query.
 */
  
DECLARE @MAX_DISTANCE AS FLOAT64 = 0.5
DECLARE @HQ_LAT AS FLOAT64 = 37.769950
DECLARE @HQ_LONG AS FLOAT64 = -122.410363
DECLARE @MORNING_START AS TIME = '07:00:00'
DECLARE @MORNING_STOP AS TIME = '10:00:00'
DECLARE @EVENING_START AS TIME = '16:00:00'
DECLARE @EVENING_STOP AS TIME = 22:00:00
 
/*
 * User defined function to calculate the distance (in miles) between two
 * points, given their longitude and latitude.  Haversine Formula.
 * Assumes longitudes and latitudes are in radians.
 */
CREATE FUNCTION Distance(lat1 FLOAT64, long1 FLOAT64, lat2 FLOAT64, long2 FLOAT64)
RETURNS FLOAT64 DETERMINISTIC
RETURN 2*3958.8*ASIN(SQRT(POW(SIN(0.5*(lat2-lat1)),2) + COS(lat1)COS(lat2)POW(SIN(0.5*(long2-long1)),2)))
  
/*
 * Query Rideshare Dataset, list the number of pickup and dropoffs that were in
 * range of HQ by date and time window.
 */
   
SELECT date1 AS [Date], morning_pickup, morning_dropoff, evening_pickup,evening_dropoff
FROM
    (SELECT DATE(pickup_time) AS date1, COUNT(pickup_time) AS morning_pickup
    FROM cruise_ride_receipts
    WHERE Distance(RADIANS(HQ_LAT), RADIANS(HQ_LONG), RADIANS(pickup_lat), RADIANS(pickup_long)) < @MAX_DISTANCE AND TIME(pickup_time) BETWEEN @MORNING_START AND @EVENING_STOP
    GROUP BY date1 )t1
FULL OUTER JOIN
    (SELECT DATE(dropoff_time) AS date2, COUNT(dropoff_time) AS morning_dropoff
    FROM cruise_ride_receipts
    WHERE Distance(RADIANS(HQ_LAT), RADIANS(HQ_LONG), RADIANS(dropoff_lat), RADIANS(dropoff_long)) < @MAX_DISTANCE AND TIME(dropoff_time) BETWEEN @MORNING_START AND @MORNING_STOP
    GROUP BY date2) t2
ON date1 = date2
FULL OUTER JOIN
    (SELECT DATE(pickup_time) AS date3 COUNT(pickup_time) AS evening_pickup
    FROM cruise_ride_receipts
    WHERE Distance(RADIANS(HQ_LAT), RADIANS(HQ_LONG), RADIANS(pickup_lat), RADIANS(pickup_long)) < @MAX_DISTANCE AND TIME(pickup_time) BETWEEN @EVENING_START AND @EVENING_STOP
    GROUP BY date3) t3
ON date2 = date3
FULL OUTER JOIN
    (SELECT DATE(dropoff_time) AS date4 COUNT(dropoff_time) AS evening_dropoff
    FROM cruise_ride_receipts
    WHERE Distance(RADIANS(HQ_LAT), RADIANS(HQ_LONG), RADIANS(dropoff_lat), RADIANS(dropoff_long)) < @MAX_DISTANCE AND TIME(dropoff_time) BETWEEN @EVENING_START AND @EVENING_STOP
    GROUP BY date4
ON date3 = date4
ORDER BY [Date]

/*
 * Comments
 * It would have been helpful to have had an example SQL dataset to try running
 * over.
 */
