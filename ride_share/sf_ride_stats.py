#!/usr/bin/env python
# Created by Christopher Anell
# Code to read in a database of ride sharing stats for the greater
# San Francisco area.
# > python3 sf_ride_stats.py "TDS_202017_20Data-WEBPAGE/Data-Table 1.csv"

import sys
from datetime import date
from operator import itemgetter
import csv
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from math import *

AGES=[(1, "18-24"),(2, "25-34"),(3,"35-44"),(4, "45-54"),(5, "55-64"),(6, "65+"),(7,"Undeclared")]
ETHNICITIES=[(1, "Asian"), (2, "African American"), (3, "Hispanic"),(4, "Caucasian"), (5, "Native American"),(6,"Other"),(7, "Undeclared"),(8, "Mixed")]
INCOMES=[(1, "$15,000 or less"),(2, "15,000 - $25,000"),(3, "25,000-35000"),(4, "35,000-75,000"),(5, "75,000 - 100,000"),(6, "100,000-200,000"),(7, "Over $200,000"), (8, "Undeclared")]
GENDERS=[(1, "Male"),(2,"Female") ,(3, "Non-Binary")]

# Code is broken into two parts: reading/formatting the data, and
# outputting statistics of interest.
def sf_ride_stats(input_file_path):
    df = read_in(input_file_path)
    output(df)

# Using the pandas module, read the Travel Survey dataset (csv).
# To understand the number of trips that Cruise Rideshare could
# compete for CarShares, TNC, and TAXI trips were combined. 
def read_in(csv_file_path):
    infile = open(csv_file_path, 'r')
    df = pd.read_csv(infile, usecols=lambda x : x.upper() in ['Q2','TRIPS','CRSHRE','TNC','TAXI','CRSHRE1', 'TNC1', 'TAXI1','WEIGHT','Q27','Q28_1','Q29','Q30'])
    infile.close()

    #
    # Format Data Frame
    # Boolean if Respondent is from SF
    df['Q2'] = df['Q2'] == 9 #Boolean
    # Sum Car Share, TNC, and TAXI
    df['RSHRE_TEMP'] = df['CRSHRE'] + df['TNC'] + df['TAXI']
    df['RSHRE_TEMP1']= df['CRSHRE1'] + df['TNC1'] + df['TAXI1']
    # Remove NaN Entries and combine'RSHRE_TEMP' and 'RSHRE_TEMP1'
    df['RSHRE']=df['RSHRE_TEMP'].apply(nan_to_zero) + df['RSHRE_TEMP1'].apply(nan_to_zero)
    return df

# For non-entries in the Data Frame, set their values equal to 0.
def nan_to_zero(x):
    if isnan(x):
        return 0
    return x

# Class to hold ridership info, # of rides, # of riders, and error on # of rides.
class Ridership:
    def __init__(self, riders, rides, rides_error):
        self.riders = riders
        self.rides = rides
        self.rides_error = rides_error
    
# Using the pandas dataframe, outputs quantities and graphs of interest.
# Part1: Distribution of rides and riders between SF and
# non-SF residents.  Calculates statistical uncertainties
# on these quantities.
# Part2: Looking at a breakdown of ridership by gender, ethnicity,
# and income levels.
def output(df):

    # PART 1
    #Total Rides By SF Residents
    is_SF=True;
    sf_ridership = ridership_by_loc(df, is_SF)
    is_SF=False
    non_sf_ridership = ridership_by_loc(df, is_SF)
    
    print("{:.1f} San Francisco riders carried out a total of {:.1f} ± {:.1f} weighted rides.".format(sf_ridership.riders, sf_ridership.rides, sf_ridership.rides_error))
    
    print("{:.1f} Non-San Francisco riders carried out a total of {:.1f} ± {:.1f} weighted rides.".format(non_sf_ridership.riders, non_sf_ridership.rides, non_sf_ridership.rides_error))
    
    # PART 2
    # Study break down of riders by age, gender, ethnicity, and income
    
    # Age
    age_percents = calc_percentages(df, "age", [x[0] for x in AGES]) #Pass just Integer IDs from AGES
    # Gender
    gender_percents = calc_percentages(df, "gen", [x[0] for x in GENDERS])
    # Income
    income_percents = calc_percentages(df, "inc", [x[0] for x in INCOMES])
    # Ethnicity
    ethnicity_percents = calc_percentages(df, "eth", [x[0] for x in ETHNICITIES])
    
    #Draw Pie Charts
    DrawPieChart("Age", [x[1] for x in AGES], age_percents) #select labels
    DrawPieChart("Gender", [x[1] for x in GENDERS], gender_percents)
    DrawPieChart("Income", [x[1] for x in INCOMES], income_percents)
    DrawPieChart("Ethnicity",[x[1] for x in  ETHNICITIES], ethnicity_percents)
    
    
# For a set of selections criteria, including sf residency, gender, income, and ethnicity.  Returns rides and weights passing selection cuts.
def Select(df, criteria, selection):
    values, weights = [], []
    if criteria == "loc":
        values, weights = df[['RSHRE', 'WEIGHT']][df['Q2']==selection].T.to_numpy()
    elif criteria == "gen":
        values, weights = df[['RSHRE', 'WEIGHT']][df['Q30']==selection].T.to_numpy()
    elif criteria == "age":
        values, weights = df[['RSHRE', 'WEIGHT']][df['Q27']==selection].T.to_numpy()
    elif criteria == "inc":
        values, weights = df[['RSHRE', 'WEIGHT']][df['Q29']==selection].T.to_numpy()
    elif criteria == "eth":
        values, weights = df[['RSHRE', 'WEIGHT']][df['Q28_1']==selection].T.to_numpy()
    else:
        values, weights = df[['RSHRE', 'WEIGHT']].T.to_numpy()
    return values, weights
    
# Function to calculate the number of riders, rides, and errors for
# the SF and non-SF residents
def ridership_by_loc(df, isSF):
    values, weights = Select(df, "loc", isSF)
    rship = Ridership(0,0,0)
    rship.riders = sum([x !=0 for x in values] * weights) #boolean array * weights
    rship.rides = sum(values * weights)
    # Assumes gaussian error on the sum, sqrt(N_weighted)*stddev (CLT).
    rship.rides_error = sqrt(sum(weights) * variance(values, weights))
    return rship
    
# Given the percentage break downs for the different possible customer groups, draw pie charts.
def DrawPieChart(title, labels, percentages):
    print("Drawing Pie Charts for " + title)
    fig1, ax1 = plt.subplots()
    ax1.pie(percentages, labels=labels, autopct='%1.1f%%',
        shadow=True, startangle=90)
    ax1.axis('equal')
    plt.title('Rides by '+title)
    plt.savefig(title+".png")

# Calculates the breakdown of ride percentages for a given criteria
def calc_percentages(df, criteria, selections):
    rides_by_criteria = [ weight_sum(Select(df, criteria, selection)) for selection in selections]
    percents = rides_by_criteria/sum(rides_by_criteria)
    return percents
    
    
# Calculates weighted sum using the weight, sum pair.  Ie the output of Select.
def weight_sum(value_weight_pair):
    return sum(value_weight_pair[0]*value_weight_pair[1])

# Given a set of values and corresponding weights, calculated the weighted
# (standard deviation)**2.
def variance(values, weights):
    average = np.average(values, weights=weights)
    variance = np.average((values - average)**2, weights=weights)
    return variance


if __name__ == '__main__':
    if len(sys.argv)!= 2:
        "Please provide input file path"
    else:
        sf_ride_stats(sys.argv[1])

# Comments
# If I was to work on this project longer, I would create a configuration file where
# the user could specify the column names and the labels for the demographic criteria,
# instead of having them declared as global variables and embeded in the code.
