/*
 * Maximum Distance (in miles)  between pickup/dropoff and the chargingstation,
 * for the ride to be counted as in range.
 */
 
 DECLARE @MAX_DISTANCE AS FLOAT64 = 0.5
 
 /*
  * User defined function to calculate the distance (in miles) between two
  * points, given their longitude and latitude.  Haversine Formula.
  * Assumes longitudes and latitudes are in radians.
  */
  CREATE FUNCTION Distance(lat1 FLOAT64, long1 FLOAT64, lat2 FLOAT64, long2 FLOAT64)
  RETURNS FLOAT64 DETERMINISTIC
  RETURN 2*3958.8*ASIN(SQRT(POW(SIN(0.5*(lat2-lat1)),2) + COS(lat1)COS(lat2)POW(SIN(0.5*(long2-long1)),2)))
  
  /*
   * Query Dataset, list the number of rides that were in range for each of the
   * charging station locations.
   */
    SELECT locations.name, COUNT(DISTINCT cruise_ride_receipts.id)
    FROM locations CROSS JOIN cruise_ride_receipts
    WHERE Distance(RADIANS(locations.lat), RADIANS(locations.long), RADIANS(cruise_ride_receipts.pickup_lat), RADIANS(crusie_ride_receipts.pickup_long)) < @MAX_DISTANCE
        OR Distance(RADIANS(locations.lat), RADIANS(locations.long), RADIANS(cruise_ride_receipts.dropoff_lat), RADIANS(crusie_ride_receipts.dropoff_long)) < @MAX_DISTANCE
    GROUP BY locations.name
    ORDER BY COUNT(DISTINCT cruise_ride_receipts.id)
    
    /*
     * Notes:
     * One disadvantage to using CROSS JOIN, is that I do not believe the final list will
     * contain charging stations with 0 rides in range.  I considered using LEFT JOIN, but
     * could not think of a straightforwad way to merge the tables.
     * Also, rides where both pickup and dropoff are within range, are counted the same as
     * rides where only pickup or dropoff is in range.
     */
